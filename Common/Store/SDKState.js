// import {observer} from "mobx-react";
import {computed, observable, action} from "mobx";
import * as Server from '../Networking/Networking';

class SDKState {

    symbols = [];
    @observable baseCurrency = null;
    @observable rates = [];

    constructor() {
        Server.fetchSymbols().then(({data}) => {
            this.symbols = data;
        })
    }


    @action _setBaseCurrency(newCurrency) {
        this.baseCurrency = newCurrency;
        this._fetchRates();
    }

    _fetchRates() {
        Server.fetchRatesBy(this.baseCurrency).then(({today, yesterday})=> {
            this.rates = Object.keys(today).map((key) => {
                const tRate = today[key];
                const yRate = yesterday[key];
                if (yRate != null) {
                    const change = ((tRate - yRate)/ yRate) * 100;
                    const country = this.symbols[key].countries[0];
                    return { country: country, rate: tRate, change: change }
                }
            });
        });
    }
}

const sdkState = new SDKState();
export default sdkState;
