import axios from 'axios';
import urlJoin from 'url-join';
import dayjs from "dayjs";

//// Axios client Instance
const client = axios.create();

//// Base URLs
const baseUrls = {
    fromNoCode : "https://v1.nocodeapi.com/assaf_yehudai_api/cx/fxgPdFevwGLGCVhH",
    fromRapidApi : "https://fixer-fixer-currency-v1.p.rapidapi.com"
}

//// Headers for RapidAPI requests
const rapidApiHeaders = {
    'x-rapidapi-key': 'c65263e565msh7ac976f71c13993p193534jsnaeec5501e2cc',
    'x-rapidapi-host': 'fixer-fixer-currency-v1.p.rapidapi.com'
}

//// End Points
const endPoints = {
   rates : "rates",
   symbols: "symbols"
};

//// Public Networking API
// --------------------------------------------------------------------------------------------
export async function fetchRatesBy(baseCurrency) {
    const [todaysRates, yesterdaysRates] = await Promise.all([
        fetchTodaysRatesBy(baseCurrency),
        fetchYesterdayRatesBy(baseCurrency)
    ]);

    const today = await todaysRates.json();
    const yesterday = await yesterdaysRates.json();

    return { today, yesterday };
}

export function fetchSymbols() {
    const url = urlJoin(baseUrls.fromNoCode, endPoints.symbols)

    return apiCall(url, "get");
}



//// Helper Methods
// --------------------------------------------------------------------------------------------
function fetchTodaysRatesBy(baseCurrency) {
    const url = urlJoin(baseUrls.fromNoCode, endPoints.rates)

    return apiCall(url, "get", {
        params: {
            source : baseCurrency
        }
    })
}

function fetchYesterdayRatesBy(baseCurrency) {
    const yesterday = dayjs().subtract(1, "day").format("YYYY-MM-DD")

    return fetchRatesByDate(yesterday, baseCurrency);
}

function fetchRatesByDate(date, baseCurrency) {
    const url = urlJoin(baseUrls.fromRapidApi, date);

    return apiCall(url, "get", {
        headers: rapidApiHeaders,
        params: {
            base: baseCurrency
        }
    });
}

//// Base Api Call
function apiCall(url, method, options) {
    const { params, headers } = options

    return client.request( {
        url: url,
        method: method,
        headers: headers,
        params: params
    });
}
